# hxGridworks
A lightweight grid operations library that supports different types of grids. Implementation for the Haxe language.

## Installation

To install a release build:

```
haxelib install hxgridworks
```

To include hxGridworks in an OpenFL project, add <haxelib name="hxgridworks" /> to your project.xml.

To add hxGridworks to a standard Haxe project, use -lib hxgridworks in your HXML
