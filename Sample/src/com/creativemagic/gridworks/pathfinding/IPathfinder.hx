package com.creativemagic.gridworks.pathfinding;
import com.creativemagic.gridworks.tile.Tile;

/**
 * @author Creative Magic
 */
interface IPathfinder 
{
	public function findPath( startTile:Tile, endTile:Tile):Array<Tile>;
	public function getDistance( startTile:Tile, endTile:Tile):Array<Tile>;
	
	public function setPathDistanceLimit(limit:Int = 0):Void;
}