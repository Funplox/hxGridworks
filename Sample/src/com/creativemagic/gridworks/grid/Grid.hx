package com.creativemagic.gridworks.grid;
import com.creativemagic.gridworks.field.IField;
import com.creativemagic.gridworks.grid.ConstructableTile;
import com.creativemagic.gridworks.grid.Tile;
import com.creativemagic.gridworks.utils.GridPoint;
import openfl.geom.Point;

/**
 * ...
 * @author Creative Magic
 */
@:generic
class Grid<T:(Tile, ConstructableTile)> 
{
	@:isVar private var gridWidth(default, null):Int; // distance between left-most tile and right-most tile
	@:isVar private var gridHeight(default, null):Int; // distance between top-most tile and bottom-most tile
	
	public var field(default, null):IField;
	public var tiles(default, null):Array<T> = [];
	
	public function new() 
	{
		
	}
	
	// GRID OPERATIONS
	
	public function createTile(position:GridPoint):T
	{
		var tile:T = new T();
		tile.posX = position.x;
		tile.posY = position.y;
		addTile(tile);
		
		return tile;
	}
	
	public function addTile(tile:T):Void
	{
		tiles.push(tile);
	}
	
	public function addTiles(tiles:Array<T>):Void
	{
		this.tiles = this.tiles.concat( tiles);
	}
	
	public function addTileAt(tile:T, targetPoint:GridPoint):Void
	{
		tile.posX = targetPoint.x;
		tile.posY = targetPoint.y;
		addTile(tile);
	}
	
	public function fillArea( a:GridPoint, b:GridPoint):Array<T>
	{
		var output:Array<T> = [];
		
		for ( i in a.x...b.x)
			for ( j in a.y...b.y)
				output.push( createTile( new GridPoint( i, j)) );
				
		return output;
	}
	
	public function removeTile(tile:T):Void
	{
		tiles.remove(tile);
	}
	
	public function removeTiles(tiles:Array<T>):Void
	{
		for (t in tiles)
			this.tiles.remove(t);
	}
	
	public function removeTilesAt(points:Array<GridPoint>):Void
	{
		for (p in points)
			removeTileAt(p);
	}
	
	public function removeTileAt(targetPoint:GridPoint):Void
	{
		tiles.remove( getGetTileAt(targetPoint) );
	}
	
	public function removeAllTiles():Void
	{
		tiles = [];
	}
	
	public function appendGrid(grid:Grid<T>, overwriteOverlappingTiles:Bool = true):Void
	{
		
	}
	
	// SEARCHING AND MATCHING
	
	public function getAreaBetweenTiles(tile1:T, tile2:T):Array<T>
	{
		return [];
	}
	
	public function getRadius(centerTile:T, radius:Int):Array<T>
	{
		return [];
	}
	
	public function getGetTileAt(point:GridPoint):T
	{
		for ( t in tiles)
			if (t.posX == point.x && t.posY == point.y)
				return t;
		
		return null;
	}
	
	public function getTileUnderPoint(point:Point):T
	{
		return null;
	}
	
	public function getNeighbours(centerTile:T):Array<T>
	{
		return [];
	}
	
	public function getTilesConnectedTo(tile:T):Array<T>
	{
		return [];
	}
	
	public function getTilesDisconnectedFrom(tile:T):Array<T>
	{
		return [];
	}
	
	public function getLineBetweenTiles(tile1:T, tile2:T):Array<T>
	{
		return [];
	}
	
	public function getTileCoordinates(tile:T):Point 
	{
		return field.getTilePosition( new GridPoint( tile.posX, tile.posY) );
	}
	
}