package com.creativemagic.gridworks.grid;

/**
 * @author Creative Magic
 */
typedef ConstructableTile =
{
	public function new():Void;	
}