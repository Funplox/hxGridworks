package com.creativemagic.gridworks.utils;

/**
 * ...
 * @author Creative Magic
 */
class GridPoint
{
	public var x:Int;
	public var y:Int;

	public function new(_x:Int = 0, _y:Int = 0) 
	{
		x = _x;
		y = _y;
	}
	
}