package com.creativemagic.gridworks.field;

import com.creativemagic.gridworks.utils.GridPoint;
import openfl.geom.Point;

/**
 * ...
 * @author Creative Magic
 */
interface IField
{
	public var tileWidth(default, null):Float;
	public var tileHeight(default, null):Float;
	public var tileEdgeLength(default, null):Float;
	
	public var tileDistance:Float;
	
	public var tileScaleX:Float;
	public var tileScaleY:Float;
	
	public function setTileSize(tileSize:TileSize):Void;
	
	public function getTileUnderPoint(point:Point):GridPoint;
	public function getTilePosition(gridPoint:GridPoint):Point;
	
	public function getArea(start:GridPoint, endPoint:GridPoint):Array<GridPoint>;
	//public function getRadius(center:GridPoint, radius:Int):Array<GridPoint>;
	//public function getSpiral(center:GridPoint, radius:Int, spiralStep:Int = 1):Array<GridPoint>;
	//public function getLine(start:GridPoint, endPoint:GridPoint):Array<GridPoint>;
	//public function getRing(center:GridPoint, offset:Int):Array<GridPoint>;
	//public function getDirection(start:GridPoint, direction:GridPoint, steps:Int):Array<GridPoint>;
	//public function getNeighbours(tile:GridPoint):Array<GridPoint>;
}