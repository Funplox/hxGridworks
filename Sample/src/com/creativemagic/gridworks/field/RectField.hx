package com.creativemagic.gridworks.field;
import openfl.geom.Point;
import com.creativemagic.gridworks.field.TileSize;
import com.creativemagic.gridworks.utils.GridPoint;

/**
 * ...
 * @author Creative Magic
 */
class RectField implements IField
{

	public function new() 
	{
		
	}
	
	
	/* INTERFACE com.creativemagic.gridworks.field.IField */
	
	public var tileWidth(default, null):Float = 10;
	public var tileHeight(default, null):Float = 10;
	public var tileEdgeLength(default, null):Float = 10;
	
	public var tileDistance:Float = 0;
	
	public var tileScaleX:Float = 1;
	public var tileScaleY:Float = 1;
	
	public function setTileSize(tileSize:TileSize):Void
	{
		switch (tileSize)
		{
			case TileSize.TileEdgeLength(l):
			{
				tileEdgeLength = l;
				tileWidth = l;
				tileHeight = l;
			}
			case TileSize.TileWidth(w):
			{
				tileEdgeLength = w;
				tileWidth = w;
				tileHeight = w;
			}
			case TileSize.TileHeight(h):
			{
				tileEdgeLength = h;
				tileWidth = h;
				tileHeight = h;
			}
		}
	}
	
	public function getTileUnderPoint(point:Point):GridPoint 
	{
		return null;
	}
	
	public function getTilePosition(gridPoint:GridPoint):Point 
	{
		var x:Float = (tileWidth + tileDistance) * gridPoint.x;
		var y:Float = (tileHeight + tileDistance) * gridPoint.y;
		return new Point(x, y);
	}
	
	
	/* INTERFACE com.creativemagic.gridworks.field.IField */
	
	public function getArea(start:GridPoint, endPoint:GridPoint):Array<GridPoint> 
	{
		var output:Array<GridPoint> = new Array<GridPoint>();
		
		for ( i in start.x...endPoint.x)
			for ( j in start.y...endPoint.y)
			{
				var point:GridPoint = new GridPoint(i, j);
				output.push(point);
			}
		
		return output;
	}
}