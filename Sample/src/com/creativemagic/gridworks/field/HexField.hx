package com.creativemagic.gridworks.field;
import com.creativemagic.gridworks.utils.GridPoint;
import openfl.geom.Point;

/**
 * ...
 * @author Creative Magic
 */
class HexField implements IField
{

	public function new() 
	{
		
	}
	
	/* INTERFACE com.creativemagic.gridworks.field.IField */
	
	public var tileWidth(default, null):Float = 10;
	public var tileHeight(default, null):Float = 10;
	public var tileEdgeLength(default, null):Float = 10;
	
	public var tileDistance:Float = 0;
	
	public var tileScaleX:Float = 1;
	public var tileScaleY:Float = 1;
	
	public function setTileSize(tileSize:TileSize) 
	{
		switch (tileSize)
		{
			case TileSize.TileEdgeLength(l):
				tileEdgeLength = l;
			case TileSize.TileWidth(w):
				tileWidth = w;
			case TileSize.TileHeight(h):
				tileHeight = h;
		}
	}
	
	public function getTileUnderPoint(point:Point):GridPoint 
	{
		return null;
	}
	
	public function getTilePosition(gridPoint:GridPoint):Point 
	{
		return null;
	}
	
}