package com.creativemagic.gridworks.field;

/**
 * @author Creative Magic
 */
enum TileSize 
{
	TileEdgeLength(l:Float);
	TileHeight(h:Float);
	TileWidth(w:Float);
}