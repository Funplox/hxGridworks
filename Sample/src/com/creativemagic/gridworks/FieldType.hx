package com.creativemagic.gridworks;

/**
 * @author Creative Magic
 */
enum FieldType 
{
	Rect;
	HexOddQ;
	HexEvenQ;
	HexOddR;
	HexEvenR;
}