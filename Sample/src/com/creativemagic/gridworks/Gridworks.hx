package com.creativemagic.gridworks;
import com.creativemagic.gridworks.field.HexField;
import com.creativemagic.gridworks.field.IField;
import com.creativemagic.gridworks.field.RectField;
import com.creativemagic.gridworks.field.TileSize;
import com.creativemagic.gridworks.grid.ConstructableTile;
import com.creativemagic.gridworks.grid.Grid;
import com.creativemagic.gridworks.grid.Tile;

/**
 * ...
 * @author Creative Magic
 */
@:access(com.creativemagic.gridworks.grid.Grid)
class Gridworks
{
	
	// Static access
	
	@:generic
	public static function CreateGrid<T:(Tile, ConstructableTile)>(fieldLayout:FieldType, tileSize:TileSize):Grid<T>
	{
		var grid:Grid<T> = new Grid<T>();
		grid.field = CreateField(fieldLayout, tileSize);
		return grid;
	}
	
	public static function CreateField(fieldLayout:FieldType, tileSize:TileSize):IField
	{	
		var field:IField = switch (fieldLayout)
		{
			case FieldType.Rect:
				new RectField();
				
			case FieldType.HexEvenQ:
				new HexField();
				
			case FieldType.HexEvenR:
				new HexField();
				
			case FieldType.HexOddQ:
				new HexField();
				
			case FieldType.HexOddR:
				new HexField();
			
			default:
				new RectField();
		}
		field.setTileSize(tileSize);
		
		return field;
	}
	
	// PUBLIC METHODS
	
	//@:generic
	//public function createGrid<T:ITile>(fieldLayout:FieldType, tileSize:TileSize):Grid
	//{
		//return CreateGridCreateField<T>(fieldLayout, tileSize);
	//}
	
	public function createField(fieldLayout:FieldType, tileSize:TileSize):IField
	{
		return CreateField(fieldLayout, tileSize);
	}
	
}