package;
import com.creativemagic.gridworks.grid.Grid;
import com.creativemagic.gridworks.grid.Tile;
import openfl.display.Sprite;

/**
 * ...
 * @author Creative Magic
 */
class SampleTile extends Tile
{
	public var s:Sprite;

	public function new() 
	{
		super();
		
		s = new Sprite();
		s.graphics.beginFill( 0xAAAAAA);
		s.graphics.lineStyle( 0, 0x00, 1);
		s.graphics.drawRect( 0, 0, 15, 15);
		s.graphics.endFill();
		
	}
}