package;

import com.creativemagic.gridworks.field.RectField;
import com.creativemagic.gridworks.field.TileSize;
import com.creativemagic.gridworks.grid.Grid;
import com.creativemagic.gridworks.utils.GridPoint;
import openfl.display.Sprite;
import openfl.geom.Point;

/**
 * ...
 * @author Creative Magic
 */
@:access(com.creativemagic.gridworks.grid.Grid)
class Main extends Sprite 
{

	public function new() 
	{
		super();
		
		var grid:Grid<SampleTile> = new Grid<SampleTile>();
		grid.field = new RectField();
		grid.field.setTileSize( TileSize.TileHeight( 15));
		grid.fillArea( new GridPoint( 0, 0), new GridPoint( 20, 20));
		
		for ( tile in grid.tiles )
		{
			var tileCoords:Point = grid.getTileCoordinates(tile);
			
			tile.s.x = tileCoords.x;
			tile.s.y = tileCoords.y;
			
			addChild(tile.s);
		}
		
		var gridPoints = grid.field.getArea( new GridPoint( 1, 2), new GridPoint(5, 5) );
		
		for ( p in gridPoints)
		{
			var t = grid.getGetTileAt( p );
			
			if (t == null)
				continue;
				
			grid.removeTile(t);
			removeChild( t.s);
		}
		
	}

}
